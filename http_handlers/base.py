from tornado.web import RequestHandler


class MainPage(RequestHandler):
    def get(self):
        self.render('main_page.html')