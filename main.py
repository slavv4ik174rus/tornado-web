import os
import json
import tornado.web
import tornado.ioloop
from http_handlers.base import MainPage


class Application(tornado.web.Application):
    _routes = [
        tornado.web.url(r"/", MainPage, name="main_page")
    ]

    def __init__(self):
        settings = {
            "static_path": os.path.join(os.path.dirname(__file__), "static"),
            "template_path": os.path.join(os.path.dirname(__file__), "template"),
            "cookie_secret": "y7ej4go57uytw3498f5otj342y9568fj4",
            "xsrf_cookies": True,
            "debug": True
        }

        print(f"""
        ######## settings ########

                secret_key: {settings['cookie_secret']},
                static_path: {settings['static_path']},
                template_path: {settings['template_path']}

        ######## settings ########
            """)

        super(Application, self).__init__(self._routes, **settings)


if __name__ == "__main__":
    app = Application()
    app.listen(port=8000)

    tornado.ioloop.IOLoop.current().start()
